docker-redis 
============

Usage
-----

Prerequisites:

 * docker needs to be installed.

Installation: `sudo docker build -t gizur/redis .`

Run the container in daemon mode: `sudo docker run -d -p 6379 prakash/redis /usr/bin/redis-server`. The 6379 ports that have been exposed from the container will be routed 
from the host to the container using the `-p` flag.


run to get redis container ip `sudo docker inspect 1f59f87ad737`


redis is working `redis-cli -h [ip-address] -p 6379`



